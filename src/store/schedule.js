import axios from "axios";
import router from "@/router";

const state={
    schedule:[]
};
const getters={
    getSchedule: (state) => state.schedule,

}
const mutations={
    setSchedule(state, payload) {
        state.schedule=payload;
      },
};
const actions={
    scheduleAction({ commit }) {
    return new Promise((resolve, reject) => {
      axios.get("https://api.football-data.org/v2/competitions/2000",
      {headers: { "X-Auth-Token": '77ebae5d362b4ec386ac06787305b265' }}
      
      ).then(
        (response) => {
          console.log(response.data.seasons)
        //   localStorage.setItem("token", response.data.token);
          resolve(response.data.seasons);
        }).catch(
        (error) => {
          reject(error);
        })
    });
  },
};
   
export default{
    state,
    mutations,
    actions,
    getters
}