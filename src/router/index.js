import Vue from 'vue'
import VueRouter from 'vue-router'



Vue.use(VueRouter)

const routes = [
 
  {
    path: '/Dashboard',
    components: {
      default:()=>import("../components/Dashboard.vue"),
      Sidebar:()=>import("../views/Sidebar.vue")
    }
  }, 
  {
    path: '/MyBookings',
    components: {
      default:()=>import("../components/MyBookings.vue"),
      Sidebar:()=>import("../views/Sidebar.vue")
    }
  }, 
  ,
  {
    path: '/AdminDashboard',
    components: {
      default:()=>import("../components/AdminDashboard.vue"),
      AdminSidebar:()=>import("../views/AdminSidebar.vue")
    }
  }, 
]

const router = new VueRouter({
  mode: 'history',
  base: process.env.BASE_URL,
  routes
})
